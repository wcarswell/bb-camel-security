package com.backbase.examples;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class MyProcessor implements Processor {
	public void process(Exchange exchange) throws Exception {
		
		try {
			String fileName = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class).replaceAll("-\\d+", "");

			exchange.getIn().setHeader(Exchange.FILE_NAME, fileName);
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
		
	}
}