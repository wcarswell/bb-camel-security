# Camel Spring Security Example

### Introduction

This example shows how to leverage the Spring Security to secure the camel endpoint.
This example also displays how to save the information to a file


### Build
You will need to compile this example first:

	mvn clean install

### Run
	mvn jetty:run

The example consumes messages from a servlet endpoint which is secured by Spring Security
with http basic authentication, there are two service:
 <http://localhost:8080/api/v1/user> is for the authenticated user whose role is ROLE_USER
 <http://localhost:8080/api/v1/admin> is for the authenticated user whose role is ROLE_ADMIN
 <http://localhost:8080/api/v1/adminprocessor> is for the authenticated user whose role is ROLE_ADMIN and demonstrates a custom processor


Then you can use the script in the client directory to send the request and check the response,
or use browser to access upper urls with the user/password
(`admin/12345` with the admin and user role  or `user/12345` with user role).

